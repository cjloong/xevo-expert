# Overview
eVo3-Expert contain implementation of a javascript expert system.

## Installation
To use library, import library from npm repository.
    
    $ npm install evo3-expert --save
    
After that, include the bundled js into your path.

    <script src="<PATH>/evo3-expert.bundle.js">

## Usage and examples

### Basic
Initialization of expert system.

    let expert = new InferenceEngine();
    



Adding a rule

    expert.addRule("Bad customer pay record")
        .sentence("Customer currently owe bank money")
        .or()
            .sentence("Customer is in bank blacklist")
            .sentence("Customer is in central bank blacklist")
        .end()
    .then()
        .globalAdd("Customer is bad customer")
    .end();

Adding a fact

    let conclusion = expert
    .sentence("Customer is bank blacklist")
    .sentence("Customer currently owe bank money")
    .exec();
    
    conclusion.show();
    
