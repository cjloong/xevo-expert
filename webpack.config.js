/**
 * This webpack configuration listens to components and apps of the system.
 */
const path = require('path');
const webpack = require("webpack");
const WebpackNotifierPlugin = require("webpack-notifier");

//Components
const component = "./src";
module.exports = [ {
	//	context : path.resolve(component)
	//	, node: {fs: "empty"}
	entry : {
		"qunit.expert" : "./src/expert/qunit.expert.js"
	}, //For faster code gen:	devtool : 'eval-source-map'
	devtool : '#source-map', //#source-map',
	// devtool: "cheap-eval-source-map",
	output : {
		path : './bundles',
		filename : "[name].bundle.js",
		sourcePrefix : ""
	},
	plugins : [
		new WebpackNotifierPlugin(),

	],
	module : {
		loaders : [ {
			test : /\.js$/,
			exclude : /(node_modules|bower_components)/,
			loader : 'babel-loader',
			query : {
				presets : [ 'es2015', 'stage-3' ]
			}
		} ]
	}
} ];
