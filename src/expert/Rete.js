//const _ = require("lodash");
import {uniq, isString, isUndefined} from "lodash";

module.exports = {};

//*******************************************************************************************
//																	RETE Network
//*******************************************************************************************
const mandatory = (p)=> {throw `Missing parameter:${p}`};
const unimplemented = (fn) => {throw `Unimplemented method:${fn}`};

module.exports.ReteNetwork = class  {
    constructor(data={
        rules: {}
        , alphaNodeIndexes: {}
        , factIndex: {} // This is to store facts that maps to alpha nodes. ie. pet.dog may map to pet.dog==charlie and pet.dog==bobby nodes
    }) {
        this.data = data;
    }

    get rules() {return this.data.rules;}
	get alphaNodeIndexes() {return this.data.alphaNodeIndexes;}
	get factIndex() {return this.data.factIndex;}


    registerRule(rule=mandatory("rule")) {
        console.debug(this.rules);
        if(this.rules.hasOwnProperty(rule.name)) throw `Duplicate rule name detected:${rule.name}`;

        this.rules[rule.name] = rule;

        let node = this._registerPredicate(rule);
        let endNode = new BetaEndpoint(rule);
        node.out.push(endNode);
        endNode.in.push(node);

    };

	// This is a routing function
    _registerPredicate(rule=mandatory("rule"), predicate) {
    	if(predicate==null){
    		var constructor = rule.predicate.constructor.name;
    		predicate = rule.predicate;
    	} else var constructor = predicate.constructor.name;
		console.debug("Registering:" + constructor);
    	switch(constructor) {
		case "PredOr": return this._registerPredOr(rule, predicate);
		case "PredAnd": return this._registerPredAnd(rule, predicate);
		case "PredSentence": return this._registerPredSentence(rule, predicate);
		case "PredFact": return this._registerPredFact(rule, predicate);
		default:
    			throw `Predicate of type:${constructor} is not supported.`;
    	}

    };
	_registerPredOr(rule=mandatory("rule"), predicate=mandatory("predicate")) {
//     	let predicate = rule.predicate;
		
		let nodes = predicate.data.predicates.map(pred=>{
			return this._registerPredicate(rule, pred);
		});

		// Linking
		let betaNode = new BetaOrNode(nodes);

		nodes.forEach(node=>{ 
			node.out.push(betaNode);
			betaNode.in.push(node);
		});
		return betaNode;
	};

	_registerPredAnd(rule=mandatory("rule"), predicate=mandatory("predicate")) {
//     	let predicate = rule.predicate;
		
		let nodes = predicate.data.predicates.map(pred=>{
			return this._registerPredicate(rule, pred);
		});
		// Linking
		let betaNode = new BetaAndNode(nodes);

		nodes.forEach(node=>{ 
			node.out.push(betaNode);
			betaNode.in.push(node);
		});
		return betaNode;

	};

	_registerPredSentence(rule=mandatory("rule"), predicate=mandatory("predicate")) {
//     	predicate = predicate || rule.predicate;
		var node = this._alphaSentenceNode(predicate.sentence);
		console.debug("registered:" + predicate.sentence);
		//node.ruleLink.push(rule);

		return node;

	}

	_registerPredFact(rule=mandatory("rule"), predicate=mandatory("predicate")) {
//     	predicate = predicate || rule.predicate;
		var fact = predicate.factStr, equalTo = predicate.equalTo;
		var node = this._alphaFactNode(fact, equalTo);

		this.factIndex[fact] = this.factIndex[fact] || [];
		var factIndexItem = this.factIndex[fact];
		if(factIndexItem.indexOf(node.key)<0) factIndexItem.push(node.key);

		console.debug("Registered:", predicate.factStr, predicate.equalTo);

		return node;

	}

	_makeFactEqKey (fact=mandatory("val"),val=mandatory("val")) {
		return `FACT:${fact}==${val}`;
	}

	_alphaFactNode(fact = mandatory("fact"), equalsTo = mandatory("equalsTo")) {
		let key = this._makeFactEqKey(fact, equalsTo);

		if(!this.alphaNodeIndexes.hasOwnProperty(key)) {
	        var newNode = new AlphaFactNode(key, fact, equalsTo);
	        this.alphaNodeIndexes[key] = newNode;
	     	console.debug("Adding:" + key);
		}

		let node = this.alphaNodeIndexes[key];
    	return node;
	}

    _alphaSentenceNode(sentence=mandatory("sentence")) {
    	if(!this.alphaNodeIndexes.hasOwnProperty(sentence)) {
	        var newNode = new AlphaSentenceNode();
	        this.alphaNodeIndexes[sentence] = newNode;
	     	console.debug("Adding:" + sentence);
    	}

    	let node = this.alphaNodeIndexes[sentence];

    	return node;
    }


    unsetSentence(sentence=mandatory("sentence")){
    	return this._sentence(sentence, false);
    }

    setSentence(sentence=mandatory("sentence")){
    	return this._sentence(sentence, true);
    }


	unsetFact(fact = mandatory("fact")) {
		return this._fact(fact);
	}


    setFact(fact=mandatory("fact"), value) {
		return this._fact(fact, value);
    }

	// We need to create alpha node for fact part, not the whole equation
	// Current method is a hack but it works
    _fact(fact=mandatory("fact"), value) {
    	let newConflictSet = [];
		const affectedNodes = [];
		const alphaNodes = this.alphaNodeIndexes;
		var currentAlphaNode;
		var on = false;
    	if(!isUndefined(value)) {
			const key = this._makeFactEqKey(fact, value);
			console.debug("key:" + key);

			if(!alphaNodes.hasOwnProperty(key)) {
				console.warn(`Unrecognized fact:[${key}]`);
				return;
			}
			currentAlphaNode = alphaNodes[key];
    		console.debug(`Setting fact[${value}] to value[${value}]`);
			currentAlphaNode.on = on = true;

//     	} else {
// 			console.debug(`Unsetting fact[${value}]`);
// 			currentAlphaNode.on = false;
			affectedNodes.push(currentAlphaNode);
    	}

		// Need to invalidate all affected nodes
		const factIndexItem = this.factIndex[fact];
		let otherAffectedNode = factIndexItem
			.filter(otherKey=>currentAlphaNode==null || currentAlphaNode.key!=otherKey)
			.map(key=>{
				let node = this.alphaNodeIndexes[key];
				node.on = on;
				return node;
			})
			;

		affectedNodes.concat(otherAffectedNode).forEach(node=>{
			debugger;
			var currentConflicts = this._walkNodes(node, on);
			newConflictSet = newConflictSet.concat(currentConflicts);
		});


// 		var currentConflicts = this._walkNodes(currentAlphaNode, true);
// 		newConflictSet = newConflictSet.concat(currentConflicts);
		let returnSet = uniq(newConflictSet);
			
		return returnSet;
    }
    
    _sentence(sentence=mandatory("sentence"), onoff=mandatory("onoff")){
		var newConflictSet = [];

    	if(isString(sentence)) sentence = [sentence];

		var alphaNodes = this.alphaNodeIndexes;
    	sentence.forEach(s=>{
    		console.debug(`setSentence_V1:s=[${s}]`);
    		console.debug(alphaNodes);
			if(!alphaNodes.hasOwnProperty(s)) {
				console.warn(`Unrecognized sentence:[${s}]`);
				return;
			}

    		var currentAlphaNode = alphaNodes[s];

			currentAlphaNode.on = onoff;
    		console.debug(`setSentence_V1:currentAlphaNode.on=[${currentAlphaNode.on}]`);

    		console.debug(`setSentence_V1:Processing sentence[${s}. It has out links#[${currentAlphaNode.out.length}]]`)
    		var currentConflicts = this._walkNodes(currentAlphaNode, onoff);
    		newConflictSet = newConflictSet.concat(currentConflicts);

//     		if(currentAlphaNode.out.length>0) {
//     			currentAlphaNode.out.forEach(node=>{
//     				if(node instanceof BetaEndpoint) newConflictSet.push(node.rule);
//     				else {
//     					let endNodesRules = this.retrieveMatchRule(node, onoff);
//     					newConflictSet = newConflictSet.concat(endNodesRules);
//     				}
//     			});
//     		}
    	});

		let returnSet = uniq(newConflictSet);
		console.debug("setSentence_V1:" + returnSet)
		return returnSet;
    }

	_walkNodes(currentNode=mandatory("currentNode"), onoff) {
		console.debug(currentNode);
		let newConflictSet = [];
				
		if(currentNode.out.length>0) {
			currentNode.out.forEach(node=>{
				if(node instanceof BetaEndpoint) newConflictSet.push(node.rule);
				else {
					let endNodesRules = this.retrieveMatchRule(node, onoff);
					newConflictSet = newConflictSet.concat(endNodesRules);
				}
			});
		}

		return newConflictSet;

	}

    retrieveMatchRule(node = mandatory("node"), onoff=mandatory("onoff")) {
		var constructor = node.constructor.name;
    	switch(constructor) {
    		case "BetaEndPoint" : 
    			console.log(`retrieveMatchRule:BetaEndPoint->${node}`)
    			return node;

    		case "BetaAndNode": 
    			var retNode = this._matchAndNode(node, onoff);
    			console.log(`retrieveMatchRule:BetaAndNode->${retNode}`)
    			return retNode;


    		case "BetaOrNode": 
    			var retNode = this._matchOrNode(node, onoff);
    			console.log(`retrieveMatchRule:BetaOrNode->${retNode}`)
    			return retNode;

    		default: throw `Logic for Constructor:[${constructor}] is not implemented.`
    	}
    	
		console.log(`retrieveMatchRule:[]`)
		return [];
    }

	_matchAndNode(node = mandatory("node"), onoff=mandatory("onoff")) {
		// Checking upstream nodes. If anyone is false, this node is false
		let found = node.in.find(node=>{
			return !node.on;
		});

		// if something found means there is a false in the AND node
		if(!isUndefined(found) && !node.on) {
// 			if(!node.on) {
				console.log("No change in AND node. Stop traversing downstream");
				console.debug(`_matchAndNode:Invalid. Found:${found}`);
				console.debug(found);
				return [];

// 			}
		}

		console.log(`_matchAndNode:AND node pass with ${node.in.length} parameters`)
		node.on=onoff;
		var matched = [];
		node.out.forEach(node=>{
			if(node instanceof BetaEndpoint) matched.push(node.rule);
			else {
				matched = matched.concat( this.retrieveMatchRule(node, onoff) );
	
			}
		});

		return matched;
	}

	_matchOrNode(node = mandatory("node"), onoff=mandatory("onoff")) {
		// Checking upstream nodes. If anyone is false, this node is false
		let found = node.in.find(node=>{
			return node.on;
		});

		if(isUndefined(found) && !node.on) {
			console.debug(`_matchOrNode:Invalid. Found:${found}`);
			console.debug(found);
			return [];
		} else {
			console.debug(`_matchOrNode:valid because of:${found}`);
			console.debug(found);
		}

		node.on=onoff;
		var matched = [];
		node.out.forEach(node=>{
			if(node instanceof BetaEndpoint) matched.push(node.rule);
			else {
				matched = matched.concat( this.retrieveMatchRule(node, onoff) );
			}
		});

		return matched;

	}

}










//*******************************************************************************************
//                                            Node
//*******************************************************************************************

class Node {
	constructor(data={
		in: []
		, out: []
		, on: false
	}) {
		this.data = data;
	}

	get in(){return this.data.in;}
	get out(){return this.data.out;}
	get on(){return this.data.on;}
	set on(cond=mandatory("on")) {
		if(cond===true || cond===false) this.data.on = cond;
	}
}
class AlphaNode extends Node  { constructor(data){super(data)} };
class AlphaSentenceNode extends AlphaNode  { constructor(data){super(data)} };
class AlphaFactNode extends AlphaNode  { 
	constructor(key = mandatory("key"), fact=mandatory("fact"), equalTo=mandatory("equalTo"), data){

		super(data);

		this.data.key = key;
		this.data.fact = fact;
		this.data.equalTo = equalTo;

	}

	get key(){return this.data.key}
	get fact(){return this.data.fact}
	get equalTo(){return this.data.equalTo}
};

class BetaNode extends Node { constructor(data){super(data)} };
class BetaAndNode extends BetaNode {
	constructor(nodes=mandatory("nodes"), data) {
		super(data);
		this.data.nodes = nodes
	}
}

class BetaOrNode extends BetaNode {
	constructor(nodes=mandatory("nodes"), data) {
		super(data);
		this.data.nodes= nodes;
	}
}

class BetaEndpoint extends BetaNode {
	constructor(rule=mandatory("rule"), data) {
		super(data);
		this.data.rule = rule;
	}
	get rule() {
		return this.data.rule;
	}

}

