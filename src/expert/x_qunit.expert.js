/*

# Knowledge Base AI
Cognitive Systems Components:-
- Metacognition
- Deliberation
- Reaction

Deliberation consist of:-
- Learning
- Reasoning
- Memory


 */
var Yaml = require('js-yaml');
var Expert = require("./expert.js").Expert;
var CondLogic = require("./expert.js").CondLogic;
var dogTrainerExpert = require("raw!./dogTrainer.yaml");
var _ = require("lodash");


class Expert2 {
	constructor(data={
		rules: []
		, variables: []
	}) {
		this.data=data;
		this.data.rules = this.processRules(data);
		this.data.variables = this.extractVariables(data);
	}

	processRules(data=null) {
		if(data==null) data = this.data; // Default to current instance data

		console.debug(data);

		let rules = data.rules.map(r=>{
			if(!_.isArray(r.if)) {
				r.if=[r.if];
			}
			return r;
		});
		return rules;
	}
	extractVariables(data=null) {
		
		if(data==null) data = this.data; // Default to current instance data

		if(data==null) throw "data is mandatory. Do pass it in for extractVariables to work.";
		if(data.rules==null && data.rules.length>0) throw "data does not have any rules.";

		let rules = data.rules;
		let variableCounts = {}
		// Looking at all rules and putting in array

		
		rules.forEach(r=>{

			r.if.map(cond=>(Object.keys(cond)[0])).forEach(v=>{
				variableCounts[v] = !variableCounts.hasOwnProperty(v)? 1 : variableCounts[v]+1;
			});
			r.then.map(cond=>(Object.keys(cond)[0])).forEach(v=>{
				variableCounts[v] = !variableCounts.hasOwnProperty(v)? 1 : variableCounts[v]+1;
			});;

			
		});

		return variableCounts;

	}
	validateAllBindings(facts, binding) {

		Object.keys(this.data.variables).forEach(varname=>{
			this.validateBindings(facts, binding, varname);
		});
	}
	//
	// This does not mutate facts
	//
	concludeIf(facts=null, binding=null, {inplace= false, keepReason= false}) {
		let originalFacts = facts; // Saved for comparison

		let reasoning = [];
		// Common validations
		this.validateAllBindings(facts, binding);

		// Extra validations
		if(facts===null) throw "Cannot conclude if no facts are given";
		if(binding===null) throw "Cannot conclude if no variables are not bound";

		// Cloning
		if(inplace===false) facts = _.cloneDeep(facts)

		//TODO: Refactor to own function (the matching function)
		// Matching all rules with facts
		let matchedRule = this.data.rules.filter(r=>{
			
			let ifCond = r.if;
			console.debug("ifCond:", ifCond);
			return this.conditionsHold(facts, binding,  ifCond);

		}).forEach(r=>{
			console.debug(r);
			
			let thenAction = r.then;
			if(thenAction == Object(thenAction) && !Array.isArray(thenAction)) thenAction = [thenAction]; // thenAction is array
			thenAction.forEach(a=>{
				let {boundFacts, boundCond, boundName} = this.computeBound(facts, binding, a, Object.keys(a)[0]);
				boundFacts.push(boundCond);

				if(keepReason) {
					var boundIf = this.replaceCondBoundVars(binding, r.if);

					reasoning.push({
						variable: boundName
						, addedFact: boundCond
						, because: boundIf
					})
				}



			})


		});

		return {facts, reasoning};
	}

	replaceCondBoundVars(binding={}, conditions=[]) {
		console.log(conditions);
		let boundRule = conditions.map(cond=>{
			return Object.keys(cond).map(varname=>{
				let result = {}
				let boundname = binding[varname]

				result[boundname] = cond[varname];
				return result;

			})[0];
		});
		return boundRule;
	}


	// Assuming AND
	conditionsHold(facts, binding, conditions=[]) {
		for(let i = 0; i<conditions.length; i++) {
			let result = this.conditionHold(facts, binding, conditions[i]);
			console.debug(Expert2.formatCondition(conditions[i], binding) + " is:" + (result===true?"True":"False"));
			if(result===false) return false;
		}

		return true;
	}

	validateBindings(facts, binding, varname) {
		if(this.data==null) throw "No data defined";
		if(this.data.rules==null) throw "No rules defined";
		if(this.data.variables==null) throw "Variables structure is not defined properly";


		let variables = Object.keys(this.data.variables) || [];
		if(variables.length==0) throw "Variables is empty";

		

		if(variables.indexOf(varname)<0) throw "[" +varname + "] is not part of any rules.";
		if(!binding.hasOwnProperty(varname)) throw "[" + varname + "] is defined in bindings.";

		const boundName = binding[varname];
		if(Object.keys(facts).indexOf(boundName)<0) throw "[" + varname + "->" + boundName + "] is not in any facts.";
	}

	computeBound(facts, binding, cond, varname) {
		let boundName = binding[varname];
		let boundFacts = facts[boundName];
		let boundCond = cond[varname];
		return {boundFacts, boundCond, boundName};
	}
	
	conditionHold(facts, binding, cond) {
		
		// Validate first
		this.validateAllBindings(facts, binding);
		
		// Ok, process condition
		if(cond == Object(cond)) {
			if(cond.and!=null) {
				// process AND condition
				throw "AND not supported yet"
			} else {
				// if nothing is defined, all keys will be evaluated with AND condition

				let varnames = Object.keys(cond) || [];
				for(let i = 0; i<varnames.length; i++) {

					// We are assuming AND, so any false will return false
					let bound = varnames[i];
					console.debug(facts);
					console.debug(binding);
					console.debug(cond);
					console.debug(bound);
					let {boundFacts, boundCond} = this.computeBound(facts, binding, cond, bound);
					if(boundFacts.indexOf(boundCond)===-1) return false;


				}

				return true;
			}
		}

	}

	static formatCondition(conditions, bindings) {
		let keys = Object.keys(condition);

		
		debugger;

		return "Hello";
	}
	static formatReasonings(reasoning=[]) {
		return reasoning.map(reason=>{
			let stmt = reason.variable + " " + reason.addedFact + " because";

			return stmt + " " + reason.because.map(rule=>{
				var reasons = Object.keys(rule).map(varname => {
					var str = varname + " " + rule[varname];
					return str;
				});

				console.log(reasons);
				let because = this.formatANDReasons(reasons);

				return because;

			});


		});
	}

	static formatANDReasons(reasons=[]) {
		if(reasons.length===0) return "";
		else if (reasons.length===1) return reasons[0];
		else {
			debugger;

		}
	}
}

function createSimpleCompoundRule() {
	let yaml = `
#
# Best Practices:
# 1. Start your sentence with a imaginary 'it'
#    example: 
rules: # Simple rules for dog trainer
- # Determine if animal or not
    if:
       and:
       - pet: is dog
       - pet: is robot
    then: 
    - pet: is robot dog

- # Normal dog    
    if:
       and:
       - pet: is dog
       - pet: 
    	   not: is robot
    then: 
    - pet: is normal dog



`.replace(/\t/g,"    ");

	return yaml;
}


function ownDogRobotExpert() {
			return  `
#
# Best Practices:
# 1. Start your sentence with a imaginary 'it'
#    example: 
rules: # Simple rules for dog trainer
- # Dog
    if:
    - pet: is dog
    then: 
    - pet: is common pet

- # Owl
    if:
    - pet: is owl
    then: 
    - pet: is exotic pet
- # Aibo the robot dog
    if: # This rule is not using array, should work as well 
      pet: is robot
    then:
    - pet: is not an animal

`.replace(/\t/g,"    ");

}


QUnit.module("Expert System", (hooks)=>{
	// Test objects
	QUnit.module("_Development",(hooks)=>{
		let E = new Expert();
		let dogOwlRobotYaml =  ownDogRobotExpert();
		let simpleCompoundRule = createSimpleCompoundRule();


		hooks.beforeEach((assert) => {
			E = new Expert();

		});



		QUnit.test("Testing for default AND rule, single level",(assert)=>{
			let rules = {
				rules: {
					if: [
						{pet: "is a dog"}
						, {pet: "stays at home"}
						, {pet: "have an owner"}
					]
					, then: [
						{pet: "is a house pet"}
					]
				}
			};

			let facts = {
				Bobby: ["is dog"]
			}
			let json = Yaml.load(dogOwlRobotYaml);
			let expert2 = new Expert2(json);
			assert.ok(facts.Bobby.length===1);
			let result = expert2.concludeIf(facts, {pet: "Bobby"}, {keepReason: true});
			assert.ok(facts.Bobby.length===1, "Original facts is unchanged");
			assert.ok(result.Bobby.length===1, "Default rules should be AND if array is encountered. So no change because not enough facts.");

			console.debug(result);

			debugger;

	
		});









		QUnit.test("Process rules. Making sure rules is the standard structure", (assert)=>{
			var json = Yaml.load(dogOwlRobotYaml);
			assert.ok(!_.isArray(json.rules[2].if), "Rule[2] is not array but will be converted");

			var expert2 = new Expert2(json);
			var result = expert2.processRules();
			assert.ok(_.isArray(json.rules[2].if), "converted to array");

		});



		QUnit.test("Human message in reasoning for evalutaion", (assert)=>{
			// Result extracted elsewhere
			let result = {  
				   "reasoning":[  
					  {  
						 "variable":"Bobby",
						 "addedFact":"is common pet",
						 "because":[  
							{  
							   "Bobby":"is dog"
							}
						 ]
					  }
				   ]
				}

				let reasons = Expert2.formatReasonings(result.reasoning);
				assert.ok(reasons.length===1);
				assert.ok(reasons[0]=="Bobby is common pet because Bobby is dog", "::" + reasons[0]);
						
		});


		QUnit.test("Evaluating simple rule for conclusion and steps (forward chaining)", (assert)=>{
			var json = Yaml.load(dogOwlRobotYaml);
			let expert2 = new Expert2(json);
			let facts = {
				"Bobby":["is dog"]
				, "Bubo":["is owl"]
			};

			let result = expert2.concludeIf(facts, {pet: "Bobby"}, {
				keepReason:true
			});
			assert.ok(result.facts.Bobby.length===2);
			assert.ok(result.facts.Bobby[0]==="is dog");
			assert.ok(result.facts.Bobby[1]==="is common pet");

			// Looking at the reasoning
			assert.ok(result.reasoning.length===1);
			assert.ok(result.reasoning[0].variable==="Bobby");
			assert.ok(result.reasoning[0].addedFact==="is common pet");
			assert.ok(result.reasoning[0].because.length===1);
			assert.ok(result.reasoning[0].because[0].Bobby==="is dog");

			console.debug(JSON.stringify(result));

			let reasons = Expert2.formatReasonings(result.reasoning);

			console.debug(reasons.join(".\n"));
			assert.ok(reasons.length===1);
			assert.ok(reasons[0]==="Bobby is common pet because Bobby is dog");

		});








		QUnit.test("Extract variables from rules", (assert)=>{
			var json = Yaml.load(dogOwlRobotYaml);
			var expert2 = new Expert2(json);
			var result = Object.keys(expert2.extractVariables());

			console.log(result);
			assert.ok(result.length===1, "" + result.length);
			assert.ok(result[0]==='pet', "" + result[0]);


		});

		QUnit.test("Validation on binding", (assert)=>{
			var json = Yaml.load(dogOwlRobotYaml);
			var expert2 = new Expert2(json);

			var facts = {
				"Bobby":["is dog"]
				, "Aibo":["is robot"]
				, "Bubo":["is owl"]
			};
			var binding = {pet: "Bubo"};

			try{
				var result = expert2.validateBindings(facts, binding, "pet");
				assert.ok(true);
			} catch (e) {
				assert.ok(false, e);
			}



		});


		QUnit.test("Evaluating simple rule for conclusion (forward chaining)", (assert)=>{
			var json = Yaml.load(dogOwlRobotYaml);
			let expert2 = new Expert2(json);

			// Loading in some facts
			let facts = {
				"Bobby":["is dog"]
				, "Aibo":["is robot"]
				, "Bubo":["is owl"]
			};

			var conclusion = expert2.concludeIf(facts, {pet: "Bobby"}).facts;
			assert.ok(conclusion.Bobby.length===2);
			assert.ok(conclusion.Aibo.length===1);
			assert.ok(conclusion.Bubo.length===1);
			assert.ok("is common pet::is dog"==conclusion.Bobby.sort().join("::"),conclusion.Bobby.sort().join("::"));
			
			var conclusion = expert2.concludeIf(facts, {pet: "Aibo"}).facts;
			assert.ok(conclusion.Bobby.length===1);
			assert.ok(conclusion.Aibo.length===2);
			assert.ok(conclusion.Bubo.length===1);
			assert.ok("is dog"==conclusion.Bobby.sort().join("::"),conclusion.Bobby.sort().join("::"));
			assert.ok("is not an animal::is robot"==conclusion.Aibo.sort().join("::"),conclusion.Aibo.sort().join("::"));

			var conclusion = expert2.concludeIf(facts, {pet: "Bubo"}).facts;
			assert.ok(conclusion.Bobby.length===1);
			assert.ok(conclusion.Aibo.length===1);
			assert.ok(conclusion.Bubo.length===2);
			assert.ok("is dog"==conclusion.Bobby.sort().join("::"),conclusion.Bobby.sort().join("::"));
			assert.ok("is robot"==conclusion.Aibo.sort().join("::"),conclusion.Aibo.sort().join("::"));
		});

		QUnit.test("Loading simple rule", (assert)=>{
			var yaml = simpleCompoundRule;

			var json = Yaml.load(yaml);
			let expert2 = new Expert2(json);
			console.debug(expert2.data);
			assert.ok(expert2.data.rules.length===2);
			
			// Checking if and then exists
			console.debug(expert2.data.rules[0]);
			var rule = expert2.data.rules[0];
			assert.ok(rule.if!=null);
			assert.ok(rule.then!=null);

			console.debug(expert2.data.rules[1]);
			var rule = expert2.data.rules[1];
			assert.ok(rule.if!=null);
			assert.ok(rule.then!=null);

			// Checking first if
			var rule = expert2.data.rules[0];
			var ifpart = rule.if;
			console.debug(ifpart); // Must contain an epression
			var ifKeys = Object.keys(ifpart[0]);
			assert.ok(ifKeys.length==1 && ifKeys[0].toLowerCase()=='and');
			var andArray = ifpart[0].and;
			console.debug(andArray);
			assert.ok(andArray.length===2);
			console.debug(andArray[0]); //{pet: "is dog"}
			assert.ok(andArray[0].pet == "is dog");

			console.debug(andArray[1]);//{pet: "is robot"}
			assert.ok(andArray[1].pet == "is robot");

			// Checking second if
			var rule = expert2.data.rules[1];
			var ifpart = rule.if[0];
			console.debug(ifpart); // Must contain an epression
			var ifKeys = Object.keys(ifpart);
			assert.ok(ifKeys.length==1 && ifKeys[0].toLowerCase()=='and');
			

		});


		QUnit.test("YAML parser test", (assert)=>{
// Goto www.json2yaml.com to edit
			let yaml = `
#
# Best Practices:
# 1. Start your sentence with a imaginary 'it'
#    example: 
rules: # Simple rules for dog trainer
- # Determine if animal or not
    if:
    and:
    - pet: is dog
    - pet: is robot
    then: 
    - pet: is robot dog

- # Determine temperament
    if: 
    - and:
        - pet: is dog
        - pet: barks
        - or:
            - pet: jumps
            - pet: bite fence
    then: 
    - pet: is fierce

- # Mad dogs
    if:
    and:
    - pet: is fierce
    - pet: is full of saliva on mouth
    then: 
    - pet: may have rabbies
- # Rule for cat
    if: 
    - pet: is cat
    then: 
    - owner: should look for cat expert
    
- # Fierce dogs
    if:
    - pet: is fierce
    then: 
    - pet: needs mouth guard

# Actions
actions:
- 
  when: should look for cat expert
  then: 
  - suggest product:
      - cat training videos
- 
  when: needs mouth guard
  then: 
  - suggest product:
      - dog mouth guard
      - dog chains
      - dog tranquilizer
-      
  when: may have rabbies
  then: 
  - suggest product:
      - dog tranquilizer
  
  
# Test cases - self diagnostics
tests:
- # Test case 1 - fierce dog
  data:
    Amelia: []
    Bobby:
    - barks
    - is dog
    - jumps
  result:
    Amelia: []
    Bobby:
    - needs mouth guard
- # Test for cat
  data:
    Amelia: []
    Bobby:
    - is cat
    - jumps
    - bark # Strange but true, a barking cat
  result:
    Amelia:
    - should look for cat expert


`.replace(/\t/g,"    ");



			console.debug(yaml);
			var obj = Yaml.load(yaml);
			console.debug(obj);

			var obj2 = Yaml.load(dogTrainerExpert);
			console.debug(obj2);
			
			assert.ok(true, "Loaded without exception");

		});

		QUnit.test("Compound and/or", (assert)=>{
			let A = CondLogic.and("a");
			let B = CondLogic.and("b");
			let notC = CondLogic.not("c");

			let OR = CondLogic.or(B, notC);

			// A . (B + !C)
			let expr = CondLogic.and(A, OR);

			assert.ok(CondLogic.eval(A,"a")===true);
			assert.ok(CondLogic.eval(A,"b")===false);
			assert.ok(CondLogic.eval(B,"b")===true);
			assert.ok(CondLogic.eval(B,"a")===false);

			assert.ok(CondLogic.eval(OR,"a")===false);
			assert.ok(CondLogic.eval(OR,"b")===false);
			assert.ok(CondLogic.eval(OR,"c")===false);
		});

		QUnit.skip("Parsing", (assert)=> {
			E.parseRule("?pet is black & ( is cat | is dog ) -> ?pet=is black pet");
			E.parseRule("?pet is black cat & has no legs -> ?owner= is an owner of a disabled black pet");
			assert.ok(false, "TODO");
		});

		QUnit.skip("Entity Property POC", (assert)=> {
			E.parseRule("?entity has >= 10 properties -> ?entity=large entity");
			E.parseRule("?entity has < 10 properties & has >= 5 properties -> ?entity=medium entity");
			E.parseRule("?entity has < 5 properties -> ?entity=small entity");
			E.define("has >= 10 properties", (x)=>(x.properties!= null && x.properties.length>=10));
			E.define("has < 10 properties", (x)=>(x.properties!= null && x.properties.length<10));
			E.define("has >= 5 properties", (x)=>(x.properties!= null && x.properties.length>=5));
		});




	});

	QUnit.module("Short Test:Primitives",(hooks)=>{
		let E = new Expert();

		hooks.beforeEach((assert) => {
			E = new Expert();
		});

		QUnit.test("AND truth table", (assert)=>{
			let TT = CondLogic.and('a', 'b');
			assert.ok(CondLogic.eval(TT, "a")===false);
			assert.ok(CondLogic.eval(TT, "b")===false);
			assert.ok(CondLogic.eval(TT, "c","d")===false);
			assert.ok(CondLogic.eval(TT, "a","b")===true);

		});
		QUnit.test("OR truth table", (assert)=>{
			let TT = CondLogic.or('a', 'b');
			assert.ok(CondLogic.eval(TT, "a")===true);
			assert.ok(CondLogic.eval(TT, "b")===true);
			assert.ok(CondLogic.eval(TT, "c","d")===false);
			assert.ok(CondLogic.eval(TT, "a","b")===true);
			
		});

		QUnit.test("NOT truth table", (assert)=>{
			let TT = CondLogic.not('a');
			assert.ok(CondLogic.eval(TT, "a")===false);
			assert.ok(CondLogic.eval(TT, "b")===true);
			assert.ok(CondLogic.eval(TT, "b","a")===false);
		});
		QUnit.test("Low level-primitive and", (assert)=>{
			// ?a is black
			// should be represented with ['a', 'is black']
			const rule = CondLogic.and("is monkey","is not hungry");
			let result = CondLogic.eval(rule, "is monkey", "is thirsty", "is not hungry");
			assert.ok(result===true);
			result = CondLogic.eval(rule, "is monkey", "is full");
			assert.ok(result===false);
			result = CondLogic.eval(rule, "is cat", "is full");
			assert.ok(result===false);

		});
		QUnit.test("Low level-primitive or", (assert)=>{
			const rule = CondLogic.or("is monkey","is not hungry");
			let result;

			result = CondLogic.eval(rule, "is monkey", "is thirsty");
			assert.ok(result===true);
			result = CondLogic.eval(rule, "is cat", "is full");
			assert.ok(result===false);
		});
		QUnit.test("Low level-primitive not", (assert)=>{
			const rule = CondLogic.not("is monkey");
			let result;

			result = CondLogic.eval(rule, "is monkey", "is thirsty");
			assert.ok(result===false);
			result = CondLogic.eval(rule, "is cat", "is monkey");
			assert.ok(result===false);
			result = CondLogic.eval(rule, "is cat", "is thirsty");
			assert.ok(result===true);
		});
    });


	QUnit.module("Short Test", (assert)=>{
		QUnit.test("Warmup",(assert)=>{
			const E = new Expert();
			assert.ok("Can init a empty object", true);
        });
    });

	QUnit.module("Medium Test", (assert)=>{
		QUnit.skip("Finished test",(assert)=>{
        });
    });

	QUnit.module("Long Test", (assert)=>{
		QUnit.skip("Finished test",(assert)=>{
        });
    });


});