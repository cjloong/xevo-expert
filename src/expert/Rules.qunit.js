const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;
const PredicateModule = require("./Predicates.js")
const PredicateBuilder = PredicateModule.PredicateBuilder;
const Predicate = PredicateModule.Predicate;

const RuleModule = require("./Rules.js");
const Rule = RuleModule.Rule;
const RuleBuilder = RuleModule.RuleBuilder;

const ActionModule = require("./Action.js");
const AddFactAction = ActionModule.AddFactAction;
const AddGlobalAction = ActionModule.AddGlobalAction;
const LoggerAction = ActionModule.LoggerAction;
const Action = ActionModule.Action;

QUnit.module("Rule", (hooks)=>{


	QUnit.module("RuleBuilder:Fact",(hooks)=>{
	    let wm;
		let rb;
		hooks.beforeEach((assert) => {
		    wm = new WorkingMemory();
		    wm
		    .facts("day.saturday", "sunny")
		    .facts("day.sunday", "sunny")
			;

			rb = new RuleBuilder();

		});
		hooks.afterEach((assert) => {
		});
		QUnit.test("Fact test", assert=>{
			const rainyRule = rb
				.rule("Rainy weekend")
					.or()
						.fact("day.sunday", "sunny")
						.fact("day.saturday", "sunny")
					.end()
				.then()
					.logRuleName()
					.globalAdd("Weekend Sunny")
					.factAdd("plan.sat", "Cook at home")
					.factAdd("plan.sun", "sleep")
				.build()
				;
			assert.ok(rainyRule!=null, `${rainyRule}`);

			// Executing
			rainyRule.forwardPropagate(wm);
			var result = wm.globalExist("Weekend Sunny");
			assert.ok(result===true, `result=${result}`);
			
			var result = wm.facts("plan.sat");
			assert.ok(result=="Cook at home", `result=${result}`);
			var result = wm.facts("plan.sun");
			assert.ok(result=="sleep", `result=${result}`);

		});

	});

	// Test objects
	QUnit.module("RuleBuilder:Global",(hooks)=>{
	    let wm;
		let rb;
		hooks.beforeEach((assert) => {
		    wm = new WorkingMemory();
		    wm
		    .global("Saturday is sunny")
		    .global("Sunday is sunny")
			;

			rb = new RuleBuilder();

		});
		hooks.afterEach((assert) => {
		});
		QUnit.test("RuleBuilder forward propagate test - notfired", assert=>{
			const rainyRule = rb
				.rule("Rainy weekend")
					.or()
						.sentence("Sunday is rainy")
						.sentence("Saturday is rainy")
					.end()
				.then()
					.logRuleName()
					.globalAdd("Weekend Rainy")
					.factAdd("plan.sat", "Cook at home")
					.factAdd("plan.sun", "sleep")
				.build()
				;
			assert.ok(rainyRule!=null, `sunnyRule=${rainyRule}`);

			// Executing
			rainyRule.forwardPropagate(wm);
			var result = wm.globalExist("Weekend Sunny");
			assert.ok(result===false, `result=${result}`);
			var result = wm.facts("plan.sat");
			assert.ok(_.isUndefined(result), `result=${result}`);
			var result = wm.facts("plan.sun");
			assert.ok(_.isUndefined(result), `result=${result}`);

		});

		QUnit.test("RuleBuilder forward propagate test - fired", assert=>{
			const sunnyRule = rb
				.rule("Sunny Weekend")
					.sentence("Sunday is sunny")
					.sentence("Saturday is sunny")
				.then()
					.logRuleName()
					.globalAdd("Weekend Sunny")
					.factAdd("plan.sat", "Go play badminton")
					.factAdd("plan.sun", "Go play soccer")
				.build()
				;
			assert.ok(sunnyRule!=null, `sunnyRule=${sunnyRule}`);

			// Executing
			sunnyRule.forwardPropagate(wm);
			var result = wm.globalExist("Weekend Sunny");
			assert.ok(result===true, `result=${result}`);
			var result = wm.facts("plan.sat");
			assert.ok(result=="Go play badminton", `result=${result}`);
			var result = wm.facts("plan.sun");
			assert.ok(result=="Go play soccer", `result=${result}`);

		});
	});

	QUnit.module("Rule Test",(hooks)=>{
	    let wm;
		let predicateBuilder;

		hooks.beforeEach((assert) => {
		    wm = new WorkingMemory();
		    wm
		    .global("Monday is sunny")
		    .global("Tuesday is sunny")
		    .global("Wednesday is sunny")
		    .global("Thursday is sunny")
		    .global("Friday is sunny")
		    .global("Saturday is sunny")
		    .global("Sunday is sunny")
			;

			predicateBuilder = new PredicateBuilder();

		});
		hooks.afterEach((assert) => {
			predicateBuilder.clear();
		});

		QUnit.test("forwardPropagate not run when predicate false", assert=>{
			var predicate = predicateBuilder.clear()
		    .sentence("Saturday is sunny")
		    .sentence("Sunday is rainy")
		    .build()
		    ;

			var goodMood = false;
			var goPlay= false;
			var goodMoodAction = new LoggerAction("Mood is good");
			var goPlayAction = new LoggerAction("Go out to play");
			var rule = new Rule("Go play rule", predicate,[goodMoodAction, goPlayAction]);
			goodMoodAction.callback=msg=>goodMood=msg=="Mood is good";
			goPlayAction.callback=msg=>goPlay=msg=="Go out to play";


			rule.forwardPropagate(wm);
			assert.ok(goodMood===false, `goodMood=${goodMood}`);
			assert.ok(goPlay===false, `goodMood=${goPlay}`);
		});

		QUnit.test("forwardPropagate ran when predicate true", assert=>{
			var predicate = predicateBuilder.clear()
		    .sentence("Saturday is sunny")
		    .sentence("Sunday is sunny")
		    .build()
		    ;

			var goodMood = false;
			var goPlay= false;
			var goodMoodAction = new LoggerAction("Mood is good");
			var goPlayAction = new LoggerAction("Go out to play");
			var rule = new Rule("Go play rule", predicate,[goodMoodAction, goPlayAction]);
			goodMoodAction.callback=msg=>goodMood=msg=="Mood is good";
			goPlayAction.callback=msg=>goPlay=msg=="Go out to play";


			rule.forwardPropagate(wm);
			assert.ok(goodMood===true, `goodMood=${goodMood}`);
			assert.ok(goPlay===true, `goodMood=${goPlay}`);

		});
		QUnit.test("Rule fire logger action", (assert)=>{
			var predicate = predicateBuilder.clear()
		    .sentence("Saturday is sunny")
		    .sentence("Sunday is sunny")
		    .build()
		    ;

			var goodMood = false;
			var goPlay= false;
			var goodMoodAction = new LoggerAction("Mood is good");
			var goPlayAction = new LoggerAction("Go out to play");
			var rule = new Rule("Rule 1", predicate,[goodMoodAction, goPlayAction]);
			goodMoodAction.callback=msg=>goodMood=msg=="Mood is good";
			goPlayAction.callback=msg=>goPlay=msg=="Go out to play";


			rule._fireActions(wm);
			assert.ok(goodMood===true, `goodMood=${goodMood}`);
			assert.ok(goPlay===true, `goodMood=${goPlay}`);

		});
		QUnit.test("Rule conflict checker", (assert)=>{
			var predicate = predicateBuilder.clear()
		    .sentence("Saturday is sunny")
		    .sentence("Sunday is sunny")
		    .build()
		    ;

			var rule = new Rule("Rule name", predicate, new LoggerAction("Mood is good"));
			var result = rule._isInConflict(wm);
			assert.ok(result===true, `result=${result}`);

			var predicate = predicateBuilder.clear()
		    .sentence("Saturday is sunny")
		    .sentence("Sunday is rainy")
		    .build()
		    ;

			var rule = new Rule("rule name", predicate, new LoggerAction("Mood is gloomy"));
			var result = rule._isInConflict(wm);
			assert.ok(result===false, `result=${result}`);

		});
    });
});

