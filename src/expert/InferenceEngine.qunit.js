let _ = require("lodash");
let WorkingMemory = require("./WorkingMemory.js").WorkingMemory;
//let makeMemento = require("./Memento.js").makeMemento;
let RuleBuilder = require("./Rules.js").RuleBuilder;
let makeMemento = require("@evve/pattern").makeMemento;
const ReteNetwork = require("./Rete.js").ReteNetwork;


/**
## Inference Engine
This currently acts as a facade to all the rest of the system.

*/
const mandatory = (p)=> {throw `Missing parameter:${p}`};
const unimplemented = (fn) => {throw `Unimplemented method:${fn}`};

class InferenceEngine {

	constructor() {
		this.data = {
			wm: new WorkingMemory()
			, rules: []
			, fired:{}
			, rete: new ReteNetwork()
		}

		this.iteration = 0;
		this.wm.trackChanges = true;

	}

	get wm() {return this.data.wm}
	get rete() { return this.data.rete}

	global(sentence) {return this.data.wm.global(sentence)}

	setSentence(sentence=mandatory("sentence")) {
		var conflictSet = [];

		this.wm.global(sentence);
		let newconflicts = this.rete.setSentence(sentence);
		var changes = [];

		do {
			this.iteration++;

			if(changes.length>0) {
				console.info(`Iteration:[${this.iteration}] -> Prev Rules execution result in new changes`);
				this._processChanges(changes, newconflicts);
			}
			

			var currentRule = this._select(conflictSet, newconflicts);
			if(currentRule!=null) {
				// Process rule
				this.wm.trackChanges = true;
				this.wm.clearChanges()

				this._execute(currentRule);

				changes = this.wm.changes;
			} else console.info(`Iteration:[${this.iteration}] -> No rules in conflict`);


			// Clearing newconflicts

		} while(currentRule!=null || conflictSet.length>0);

		//
	}
	unsetSentence(sentence=mandatory("sentence")) {


	}

	_processChanges(changes=mandatory("changes"), newconflicts=mandatory("newconflicts")) {
		changes.forEach(change=>{
			console.debug(`Iteration:[${this.iteration}]=>Routing changes:${change}`);
			console.debug(newconflicts);
			console.debug(change);

			switch(change.scope.trim().toLowerCase()) {
			case "global":
				return this._processGlobalChange(change, newconflicts);
			default:
				console.error(`Unimplemented scope:${change.scope}`);
			}

		});
	}

	_processGlobalChange(change=mandatory("change"), newconflicts=mandatory("newconflicts")) {
			switch(change.tx.trim().toLowerCase()) {
			case "add":
				let conflicts = this.rete.setSentence(change.data);
				newconflicts = newconflicts.concat(conflicts);
				return;
			default:
				console.error(`Unimplemented tx:${change.tx}`);
			}

	}

	_execute(rule=mandatory("rule")) {
			
			console.debug(`Iteration:[${this.iteration}]=>Executing Rule:${rule.name}`);
			rule._fireActions(this.data.wm);


	}
	_select(conflictSet=mandatory("conflictSet"), newConflicts=mandatory("newConflicts")) {
			if(newConflicts.length===0) {
				console.info(`Iteration:[${this.iteration}]=>No conflicts detected, working on old conflict set`);
				var currentRule = conflictSet.shift();

			} else {
				console.debug(`Iteration:[${this.iteration}]=>Selecting rule base on recency.`);
				console.info(`Iteration:[${this.iteration}]=>${newConflicts.length} conflicts detected.`)
				console.debug(newConflicts);

				var currentRule = newConflicts.shift();
				conflictSet = conflictSet.concat(newConflicts);

				console.info(`Iteration:[${this.iteration}]=>Processing rule from newconclicts`);
			}

			return currentRule;

	}


	// DEPRECATE:BEGIN
	_match() {
		var self = this;
		// Currently we use brute force matching
		let conflictSet = this.data.rules.filter((rule)=>{
			return self.data.fired[rule.name]!==true && rule._isInConflict(self.data.wm);
		});

		return conflictSet;
	}

	_resolve(conflictSet=[]) {
		if(conflictSet.length===0) return null;
		else if (conflictSet.length===1) return conflictSet[0];
		else {
			// TODO: Resolution algorithm

			return conflictSet[0];
		}
	}

	_fireRule(rule) {
		if(rule!=null)  {
			rule._fireActions(this.data.wm);
			this.data.fired[rule.name]=true;
		}


	}

	forwardChain() {
		do {

			var conflictSet = this._match();
			var rule = this._resolve(conflictSet);
			this._fireRule(rule);

		} while (rule!=null);

	}
	// DEPRECATE:BEGIN



	// Rule building part
	get _ruleBuilder() {
		if(this.__ruleBuilder==null) this.__ruleBuilder = new RuleBuilder();

		return this.__ruleBuilder;
	}

	get _rete() {
		return this.data.rete;
	}
	// Facade to _ruleBuilder to add rule
	rule(name=null) {
		this._ruleBuilder.rule(name);
		return this;
	}

	end() {
		this._ruleBuilder.end();
		return this;
	}
	or() {
		this._ruleBuilder.or();
		return this;
	}
	and() {
		this._ruleBuilder.and();
		return this;
	}
	sentence(sentence = null) {
		this._ruleBuilder.sentence(sentence);
		return this;
	}
	then() {
		this._ruleBuilder.then();
		return this;
	}
	logRuleName() {
		this._ruleBuilder.logRuleName();
		return this;
	}
	globalAdd(sentence) {
		this._ruleBuilder.globalAdd(sentence);
		return this;
	}
	factAdd(fact, value) {
		this._ruleBuilder.factAdd(fact, value);
		return this;
	}

	endRule() {
		let rule = this._ruleBuilder.build();
		this.data.rules.push(rule);

		// Link to Rete Network
		this._rete.registerRule(rule);

		return this;
	}

}


QUnit.module("Inference Engine", (hooks)=>{
	

	QUnit.module("Basic global rules",(hooks)=>{
		let engine;

		function registerAnimalRule() {
			engine
			.rule("Animal is dangerous")
				.or()
					.sentence("Animal have sharp claws")
					.sentence("Animal have sharp teeth")
					.sentence("Animal eat meat")
				.end()
				.sentence("Animal is hungry")
			.then()
				.logRuleName()
				.factAdd("Animal", "is dangerous")
				.globalAdd("Animal is dangerous")
			.endRule()
			.rule("Animal is cute")
				.or()
					.sentence("Animal have blunt claws")
					.sentence("Animal have blunt teeth")
					.sentence("Animal eat plant")
				.end()
				.sentence("Animal is hungry")
			.then()
				.logRuleName()
				.factAdd("Animal", "is cute")
				.globalAdd("Animal is cute")
			.endRule()

		}
		hooks.beforeEach((assert) => {
			engine = new InferenceEngine();
		});

		QUnit.test("2 Rule fire - cute and dangerous", (assert)=>{
			registerAnimalRule();

			engine.setSentence("Animal have blunt claws");
			assert.ok(engine.global().length===1, `${engine.global().length}`);

			engine.setSentence("Animal eat meat");
			assert.ok(engine.global().length===2, `${engine.global().length}`);


			engine.setSentence("Animal is hungry");
			console.debug(engine.global());
			assert.ok(engine.global().length===5, `${engine.global().length}`);
			assert.ok(engine.global()[0]=="Animal have blunt claws", `${engine.global()[0]}`);
			assert.ok(engine.global()[1]=="Animal eat meat", `${engine.global()[1]}`);
			assert.ok(engine.global()[2]=="Animal is hungry", `${engine.global()[2]}`);
			assert.ok(engine.global()[3]=="Animal is dangerous", `${engine.global()[3]}`);
			assert.ok(engine.global()[4]=="Animal is cute", `${engine.global()[4]}`);

		});

		QUnit.test("Warmup test", (assert)=>{
			registerAnimalRule();
			engine.setSentence("Animal have blunt claws");
			assert.ok(engine.global().length===1, `${engine.global().length}`);
			engine.setSentence("Animal is hungry");
			console.debug(engine.global());
			assert.ok(engine.global().length===3, `${engine.global().length}`);
			assert.ok(engine.global()[0]=="Animal have blunt claws", `${engine.global()[0]}`);
			assert.ok(engine.global()[1]=="Animal is hungry", `${engine.global()[1]}`);
			assert.ok(engine.global()[2]=="Animal is cute", `${engine.global()[2]}`);

		});
	});
});

/* Deprecate:BEGIN
QUnit.module("Inference Engine", (hooks)=>{
	

	QUnit.module("Init rete network",(hooks)=>{
		let wm;
		let engine;
		let rete;

		hooks.beforeEach((assert) => {
			engine = new InferenceEngineRete(rete);
			
		});

		QUnit.skip("Testing for working memory linking with rete network", (assert)=>{
			debugger;
			engine
			.rule("Good weekend")
				.sentence("Sat is sunny")
				.sentence("Sun is sunny")
			.then()
				.logRuleName()
				.globalAdd("Weekend sunny")
				.factAdd("weekend.activity", "Go out")
			.endRule()
			.rule("Good weekend")
				.or()
					.sentence("Sat is rainy")
					.sentence("Sun is rainy")
				.end()
			.then()
				.logRuleName()
				.globalAdd("Weekend rainy")
				.factAdd("weekend.activity", "Stay at home")
			.endRule()

			;

		});
	});



	QUnit.module("Internals",(hooks)=>{
		let wm;
		let engine;

		hooks.beforeEach((assert) => {
			wm = new WorkingMemory();
			engine = new InferenceEngine();

		});

		QUnit.test("Check rule builder", (assert)=>{
			const rb = engine._ruleBuilder;
			assert.ok(rb!=null, rb);
			assert.ok(rb instanceof _ruleBuilder, rb);
		});
	});
	
	QUnit.module("_Development",(hooks)=>{
		let wm;
		let engine;

		hooks.beforeEach((assert) => {
			wm = new WorkingMemory();
			engine = new InferenceEngineRete();
			
			engine
			.rule("Good weekend")
				.or()
					.sentence("Have friends coming")
					.sentence("Have family coming")
				.end()
				.sentence("Sat is sunny")
			.then()
				.logRuleName()
			.endRule()
			.rule("Bad weekend")
				.or()
					.sentence("Sat is rainy")
					.sentence("Sun is rainy")
				.end()
				.sentence("Alone at home")
			.then()
				.logRuleName()
			.endRule()
			;


		});

		QUnit.test("Test good weekend", (assert)=>{
			wm
			.global("Sat is sunny")
			.global("Sun is sunny")
			;
			engine.initWorkingMemory(wm);
			engine.forwardChain();

			var result = wm.globalExist("Weekend rainy");
			assert.ok(result===false, `result=${result}`);
			var result = wm.globalExist("Weekend sunny");
			assert.ok(result===true, `result=${result}`);
		});

		QUnit.test("Test rainy weekend", (assert)=>{
	
			wm
			.global("Sat is sunny")
			.global("Sun is rainy")
			;

			console.log(wm.global().length);
			assert.ok(wm.global().length===2, wm.global().length);

			engine.initWorkingMemory(wm);
			engine.forwardChain();

			var result = wm.globalExist("Weekend rainy");
			assert.ok(result===true, `result=${result}`);
			var result = wm.globalExist("Weekend sunny");
			assert.ok(result===false, `result=${result}`);


		});
	});


});


Deprecate:END */