//REFACTORING:BEGIN
// Should refactor these out of here. This is the main qunit test

//REFACTORING:END

require('./WorkingMemory.qunit.js');
require('./Memento.qunit.js');
require('./Predicates.qunit.js');
require('./Rules.qunit.js');
require('./InferenceEngine.qunit.js');
require('./Action.qunit.js');
require('./Rete.qunit.js');

QUnit.module("Experiments", (hooks)=>{

	//*******************************************************************************************
	//                                            Under Development
	//*******************************************************************************************
	QUnit.module("Immutability-ES7",(hooks)=>{

        QUnit.test("Class members", assert=>{
            class A {
                constructor() {this.test=1}
            }
            let a = new A;
            assert.ok(a.test===1);
        })

	   QUnit.test("Static Class members", assert=>{
            class A {
                constructor() {this.test=1}
				static a(...args) {
					return args;
				}
			}

			const a = A.a(1,2, 4);
			console.log(a)
			assert.deepEqual(a, [1,2,4]);


		})

	})

	QUnit.module("Immutability-ES7",(hooks)=>{
        QUnit.test("Using object spread changing value (ES7 - stage 3 at the time the test was written)", assert=>{
            const a = {name:"Alfred", sex:"Male", settings: [1,2]}
            const b = { ... a, name:"Simon"} // NOTE: Destructure first, then mutate. If the other way, the destructure will override mutation

            console.debug(JSON.stringify(a))
            console.debug(JSON.stringify(b))
            assert.deepEqual(a, {"name":"Alfred","sex":"Male","settings":[1,2]})
            assert.deepEqual(b, {"name":"Simon","sex":"Male","settings":[1,2]})

        })
        QUnit.test("Using object spread (ES7 - stage 3 at the time the test was written)", assert=>{
            const a = {name:"Alfred", sex:"Male", settings: [1,2]}
            const b = { ... a}
            b.name = "Simon"
            b.settings.push(3)

            // Array is still referencing the same object, therefore the same
            console.debug(JSON.stringify(a))
            console.debug(JSON.stringify(b))
            assert.deepEqual(a, {"name":"Alfred","sex":"Male","settings":[1,2,3]})
            assert.deepEqual(b, {"name":"Simon","sex":"Male","settings":[1,2,3]})

            b.settings = b.settings.concat(4)
            b.settings.push(5)
            a.settings.push(6)
            console.debug(JSON.stringify(a))
            console.debug(JSON.stringify(b))
            assert.deepEqual(a, {"name":"Alfred","sex":"Male","settings":[1,2,3,6]})
            assert.deepEqual(b, { "name":"Simon","sex":"Male","settings":[1,2,3,4,5]})


        })

	})

	QUnit.module("Immutability",(hooks)=>{

        QUnit.test("Object and array", assert=>{
            var a = {name:"Will", things: [0,1,2]}
            var b = Object.assign({}, a, {name:"Fred"});

            assert.deepEqual(a, {"name":"Will","things":[0,1,2]})
            assert.deepEqual(b, {"name":"Fred","things":[0,1,2]})
            // NOTE: b.things is still referencing a.things
            a.things.push(3);
            assert.deepEqual(a, {"name":"Will","things":[0,1,2,3]})
            assert.deepEqual(b, {"name":"Fred","things":[0,1,2,3]})

            // If we need immutability on array in object we need concat again
            b.things = a.things.concat(4);
            a.things.push(5);
            assert.deepEqual(a, {"name":"Will","things":[0,1,2,3,5]})
            assert.deepEqual(b, {"name":"Fred","things":[0,1,2,3,4]})

            // NOTE: Object.assign, concat, filter, map, reduce always return new object
        });

		QUnit.test("For array - filter", (assert)=>{
		    var a = [0,1,2]
		    var b = a.filter(v=>v!==2);
            console.log(a);
            console.log(b);
		    assert.deepEqual(a, [0,1,2]);
		    assert.deepEqual(b, [0,1,]);


		});

		QUnit.test("For array - insert", (assert)=>{
		    var a = [0,1,2]
		    var b = a.concat(3)
            console.log(a);
            console.log(b);
		    assert.deepEqual(a, [0,1,2]);
		    assert.deepEqual(b, [0,1,2,3]);


		});

		QUnit.test("For object, use Object.assign", (assert)=>{
            var a = {name:"Will", age: 35};
            var b = Object.assign({}, a, {name:"Alfred"}, {sex:"M"});
            console.log(a);
            console.log(b);
            assert.deepEqual(b, {name: "Alfred", age: 35, sex: "M"})
            assert.deepEqual(a,  {name: "Will", age: 35})
		})
	});

	QUnit.module("Js Lang Experiements: string",(hooks)=>{

		QUnit.test("Failed return because of semicolor replacement", (assert)=>{
		    function a() {
		        return
		          1;
		    }
		    function b() {
		        return (
		          1
                );
		    }

            console.log(a());
		    assert.ok(a()==null);
		    assert.ok(b()===1);

		});
		QUnit.test("Trailing commas", (assert)=>{
		    var a = [1,2,3,];
		    assert.ok(a.length!==4);
		    assert.ok(a.length===3);

		    var b = {a:1, b:2, c:3,}
		    assert.ok(Object.keys(b).length!==4);
		    assert.ok(Object.keys(b).length===3);
		});
		QUnit.test("Primitive Types variables", (assert)=>{
		    var a = "Alfred";
		    var b = a;
		    a = "Benjamin";
		    assert.ok(a=="Benjamin", `1. ${a}`);
		    assert.ok(b=="Alfred", `1. ${b}`);
		})

		QUnit.test("Primitive Types variables: number", (assert)=>{
		    var a = "Alfred";
		    var b = a;
		    a = "Benjamin";
		    assert.ok(a=="Benjamin", `1. ${a}`);
		    assert.ok(b=="Alfred", `1. ${b}`);
		})

		QUnit.test("Reference Types variables: array", (assert)=>{
		    var a = ["Alfred"];
		    var b = a;
		    a.push("Benjamin");
		    assert.ok(a[0]=="Alfred", `1. ${a[0]}`);
		    assert.ok(a[1]=="Benjamin", `1. ${a[1]}`);
		    assert.ok(b[0]=="Alfred", `1. ${b[0]}`);
		    assert.ok(b[1]=="Benjamin", `1. ${b[1]}`);
		})

	});
})



