const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;

//
//								Action Section
/**
# Action
Uses command pattern
*/
class Action {
	constructor(param={
		name: "?"
	}) {
		this.name = param.name;
	}

	exec(wm) {
		throw "exec() method needs to be overriden";
	}
}

class LoggerAction extends Action {
	constructor(msg="Logger Action Called(default message)", param={
		callback: console.log
	}) {
		super(param);
		this.msg = msg;
		this.callback = param.callback;
	}

	exec(wm) {
		this.callback(this.msg);
	}
}

class AddGlobalAction extends Action {
	constructor(fact=null, param={
	}) {
		super(param);
		if(fact==null) throw "fact is mandatory";

		this.fact = fact;
	}

	exec(wm) {
		if(wm==null) throw "wm is mandatory";

		wm.global(this.fact);
	}
}

class AddFactAction extends Action {
	constructor(fact=null, value, param={
	}) {
		super(param);
		if(fact==null) throw "fact is mandatory";
		if(_.isUndefined(value)) throw "value is mandatory. It can be null if you want to clear the value";

		this.fact = fact;
		this.value = value;
	}

	exec(wm) {
		if(wm==null) throw "wm is mandatory";

		wm.facts(this.fact, this.value);
	}

}

module.exports = {}
module.exports.AddFactAction = AddFactAction;
module.exports.AddGlobalAction = AddGlobalAction;
module.exports.LoggerAction = LoggerAction;
module.exports.Action = Action;
