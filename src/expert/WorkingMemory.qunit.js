var WorkingMemory = require("./WorkingMemory.js").WorkingMemory;

//
//                                             WORKING MEMORY SECTION
//

QUnit.module("Working Memory", (hooks)=>{
	// Test objects
	
	QUnit.module("_Development",(hooks)=>{

		QUnit.skip("factsDelete", (assert)=>{});
		QUnit.skip("factsModify", (assert)=>{});

		QUnit.test("Track changes", (assert)=>{
			const wm = new WorkingMemory();
			wm.trackChanges = true;

			wm.global("A");
			assert.ok(wm.changes.length===1, `${wm.changes.length}`);
			wm.global("A");
			assert.ok(wm.changes.length===1, `${wm.changes.length}`);
			wm.clearChanges()
			assert.ok(wm.changes.length===0, `${wm.changes.length}`);
			wm.global("A");
			assert.ok(wm.changes.length===0, `A exist so no increase. ${wm.changes.length}`);
			wm.global("B");
			assert.ok(wm.changes.length===1, `${wm.changes.length}`);
			wm.global("C");
			assert.ok(wm.changes.length===2, `${wm.changes.length}`);
		});

		QUnit.test("factsAdd", (assert)=>{
			const wm = new WorkingMemory();
			let result;

			result = wm.size();
			assert.ok(result===0, `wm.size()==${result}`);

			// Adding a facts
			wm
			.factsAdd("pet.bobby.gender", "female")
			.factsAdd("pet.rocky.gender", "male")
			.factsAdd("pet.rocky.breed", "St.Bernard")
			;

			result = wm.factsGet("pet.bobby.gender");
			assert.ok(result==="female", `result=${result}`);
			result = wm.factsGet("pet.rocky.gender");
			assert.ok(result==="male", `result=${result}`);
			result = wm.factsGet("pet.rocky");
			assert.ok(result.breed==="St.Bernard", `result=${result}`);
			result = wm.factsGet("pet.bobby.breed");
			assert.ok(_.isUndefined(result), `result=${result}`);
			result = wm.factsGet("pet.rocky.breed");
			assert.ok(result==="St.Bernard", `result=${result}`);
		});

		QUnit.test("deleteGlobal", (assert)=>{
			const wm = new WorkingMemory();
			wm.global("Today is sunny")
			.global("Temperature is high")
			.global("Humidity is low")
			;
			var result = wm.size();
			assert.ok(result===3, `result=${result}`);


			result = wm.globalExist("  Today is sunny  ");
			assert.ok(result===true, `result=${result}`);
			wm.globalDelete("  Today is sunny  ");
			result = wm.size();
			assert.ok(result===2, `result=${result}`);
			result = wm.globalExist("  Today is sunny  ");
			assert.ok(result===false, `result=${result}`);
		});

		QUnit.test("globalExist", (assert)=>{
			const wm = new WorkingMemory();
			var result = wm.globalExist("Today is sunny");
			assert.ok(result===false, `Result:${result}`);

			wm.global("Today is sunny");
			var result = wm.globalExist("Today is sunny");
			assert.ok(result===true, `Result:${result}`);

		});

		QUnit.test("Add to working memory", (assert)=>{
			const wm = new WorkingMemory();
			assert.ok(wm.size()===0, `size=${wm.size()}`);
			wm.global("Today is a sunny day");
			assert.ok(wm.size()===1, `size=${wm.size()}`);
			wm.global("Temperature is high");
			assert.ok(wm.size()===2, `size=${wm.size()}`);

			wm.global("Temperature is high");
			assert.ok(wm.size()===2, `fact not added because duplicated: size=${wm.size()}`);

			var global = wm.global();
			assert.ok(global.length===2, `Global length=${global.length}`);
			assert.ok(global[0]=="Today is a sunny day", `Content[0]=${global[0]}`);
			assert.ok(global[1]=="Temperature is high", `Content[1]=${global[1]}`);
		});
		QUnit.test("Empty Working memory initialization", (assert)=>{
			const wm = new WorkingMemory();
			assert.ok(wm.size()===0, `size=${wm.size()}`);
		});
	});

});


