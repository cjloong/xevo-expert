// const _ = require("lodash");

import {RuleModule, Rule, RuleBuilder} from "./Rules.js";

// const RuleModule = require("./Rules.js");
// const Rule = RuleModule.Rule;
// const RuleBuilder = RuleModule.RuleBuilder;
const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;
const ReteNetwork = require("./Rete.js").ReteNetwork;



QUnit.module("ReteNetwork", (hooks)=>{
	
	//*******************************************************************************************
	//                                            Under Development
	//*******************************************************************************************
	QUnit.module("Fact network",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});

		QUnit.skip("Basic test - set fact", (assert)=>{
		    var rule = rb
		    .rule("Rule fired")
		    	.fact('pet.animal', 'dog')
		    	.fact('pet.gender', 'male')
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.setFact("pet.animal", "dog");
			assert.ok(result.length===0, `1. result.length=${result.length}`);

			var result = rete.setFact("pet.gender", "male");
			assert.ok(result.length===1, `2. result.length=${result.length}`);
			assert.ok(result[0].name=="Rule fired", `3. result=${result[0].name}`);
			
			var result = rete.unsetFact("pet.animal");
			assert.ok(result.length===1, `4. result.length=${result.length}`);
			assert.ok(result[0].name=="Rule fired", `5. result=${result[0].name}`);

debugger;

			var result = rete.unsetFact("pet.gender");
			assert.ok(result.length===0, `6. result.length=${result.length}`);
			console.debug(result);
		});
	});
});

QUnit.module("ReteNetwork", (hooks)=>{
	
	//*******************************************************************************************
	//                                            Under Development
	//*******************************************************************************************
	QUnit.module("_Development",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});

		QUnit.test("Unset test", (assert)=>{
		    var rule = rb
		    .rule("Rule fired")
		    	.sentence('A')
		    	.sentence('B')
            .then().logRuleName().build();
            rete.registerRule(rule);
			var result = rete.setSentence("A");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("B");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Rule fired", `result=${result[0].name}`);

			var result = rete.unsetSentence("A");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Rule fired", `result=${result[0].name}`);

			var result = rete.unsetSentence("B");
			assert.ok(result.length===0, `result.length=${result.length}`);

			var result = rete.setSentence("A");
			assert.ok(result.length===0, `result.length=${result.length}`);

			var result = rete.setSentence("B");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Rule fired", `result=${result[0].name}`);

		});

	});
	
	QUnit.module("Multilevel setting sentences test",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});

		QUnit.test("Multilevel test - Many levels. Set 1 by 1", (assert)=>{
		    var rule = rb
		    .rule("Rule fired")
				.sentence("A")
				.sentence("B")
				.and()
					.or()
						.sentence("C")
						.and()
							.sentence("D")
							.sentence("E")
							.sentence("F")
						.end()
					.end()
					.sentence("G")
					.sentence("H")
				.end()
				.and()
					.sentence("I")
					.sentence("J")
					.sentence("K")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.setSentence("A");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("B");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("D");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("E");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("F");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("G");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("H");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("I");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("J");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("K");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Rule fired", `result=${result[0].name}`);

		});

		QUnit.test("Multilevel test - complex test 2 level, AND(a,b,AND(c,d),AND(e,f)). Set 1 by 1", (assert)=>{
		    var rule = rb
		    .rule("Good weekend")
				.sentence("Sat sunny")
				.sentence("Sun sunny")
				.and()
					.sentence("Friends around")
					.sentence("Brothers around")
				.end()
				.and()
					.sentence("There is booz")
					.sentence("There is bbq")
					.sentence("There is meat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.setSentence("Sat sunny");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("Sun sunny");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("Friends around");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("Brothers around");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("There is booz");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("There is bbq");
			assert.ok(result.length===0, `result.length=${result.length}`);
			var result = rete.setSentence("There is meat");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Good weekend", `result=${result[0].name}`);

		});
		
		QUnit.test("Multilevel test - complex test 2 level, AND(a,b,OR(c,d),OR(e,f))", (assert)=>{
		    var rule = rb
		    .rule("Good weekend")
				.sentence("Sat sunny")
				.sentence("Sun sunny")
				.or()
					.sentence("Friends around")
					.sentence("Brothers around")
				.end()
				.or()
					.sentence("There is booz")
					.sentence("There is bbq")
					.sentence("There is meat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);
			var result = rete.setSentence(["Sat sunny", "Sun sunny", "Brothers around", "There is booz"]);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Good weekend", `result.length=${result[0].name}`);


		});
	});

	
	
	
	
	
	
	//*******************************************************************************************
	//                                            AND Rules
	//*******************************************************************************************
	
	QUnit.module("AND Test",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});

		QUnit.test("AND test enough rules", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
				.sentence("A roars")
				.sentence("A growls")
				.sentence("A has sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is kind")
				.sentence("A purrs")
				.sentence("A eat vege")
				.sentence("A no sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

			assert.ok(true,"Testing not enough rules");
			var result = rete.setSentence(["A roars", "A growls", "A has sharp teeth"]);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is fierce", `result.length=${result[0].name}`);
		});

		QUnit.test("AND test not enough rules", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
				.sentence("A roars")
				.sentence("A growls")
				.sentence("A has sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is kind")
				.sentence("A purrs")
				.sentence("A eat vege")
				.sentence("A no sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

			assert.ok(true,"Testing not enough rules");
			var result = rete.setSentence(["A roars", "A growls"]);
			assert.ok(result.length===0, `result.length=${result.length}`);
		});
	});

	
	
	
	
	
	
	
	//*******************************************************************************************
	//                                            OR Rules
	//*******************************************************************************************
	
	QUnit.module("OR Rules",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});


		QUnit.test("Matcher:OR suppoort-match with 2 rule - 1 match", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
		    	.or()
                	.sentence("A roars")
                	.sentence("A growls")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is cat family")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
                	.sentence("A is a cat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);


			var result = rete.setSentence(["A roars", "A growls"]);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is fierce", `result=${result[0].name}`);

		});

		QUnit.test("Matcher:OR suppoort-match with 2 rule - 2 match", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is cat family")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
                	.sentence("A is a cat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);


			var result = rete.setSentence(["A is a tiger", "A is a lion"]);
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is fierce", `result=${result[0].name}`);
			assert.ok(result[1].name=="A is cat family", `result=${result[1].name}`);

		});

		QUnit.test("Matcher:OR suppoort - single rule", (assert)=>{
		    var rule = rb
		    .rule("A is vicious")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.setSentence("A is a tiger");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is vicious", `result=${result[0].name}`);

			var result = rete.setSentence("A is a lion");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is vicious", `result=${result[0].name}`);


			var result = rete.setSentence("A is a cat");	
			assert.ok(true, "No Exception expected");

		});



	});

	
	
	
	
	//*******************************************************************************************
	//                                            Simple Tests
	//*******************************************************************************************
	QUnit.module("Simple sentence",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});




		QUnit.test("Matcher:setSentence(sentence)", (assert)=>{
		    var rule = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A has sharp claws")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.setSentence("A is a tiger");
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is vicious", `result=${result[0].name}`);
			assert.ok(result[1].name=="A has sharp claws", `result=${result[1].name}`);

		});

		QUnit.test("Register empty rule", (assert)=>{
		    try{
    		    rete.registerRule();
    		    assert.ok(false, "Should have exception");
		    } catch (e) {
		        assert.ok(true, `Exception:${e}`);
		        assert.ok("Missing parameter:rule"==e, `Checking ${e}`);
		    }

		});
		QUnit.test("Duplicate rule registered", (assert)=>{
		    let rule1 = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule1);

		    try{
	            rete.registerRule(rule1);
    		    assert.ok(false, "Should have exception");
		    } catch (e) {
		        assert.ok(true, `Exception:${e}`);
		        assert.ok("Duplicate rule name detected:A is vicious"==e, `Checking ${e}`);
		    }

		});


		QUnit.test("Register a repeated predicate rule", (assert)=>{
		    var rule = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A has sharp claws")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A has sharp teeth")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = Object.keys(rete.rules);
			assert.ok(result.length===3, `result.length=${result.length}`);
			assert.ok(result[0]=="A is vicious", `result=${result[0]}`);
			assert.ok(result[1]=="A has sharp claws", `result=${result[1]}`);
			assert.ok(result[2]=="A has sharp teeth", `result=${result[2]}`);

			var result = Object.keys(rete.alphaNodeIndexes);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0]=="A is a tiger", `result=${result[0]}`);

			var result = rete.alphaNodeIndexes["A is a tiger"].out
			assert.ok(result.length===3, result.length);
			assert.ok(result[0].rule.name=="A is vicious", result[0].rule.name);
			assert.ok(result[1].rule.name=="A has sharp claws", result[1].rule.name);
			assert.ok(result[2].rule.name=="A has sharp teeth", result[2].rule.name);

		});

		QUnit.test("Register a simple rule", (assert)=>{
		    
		    let rule1 = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule1);

		    let rule2 = rb
		    .rule("A is gentle")
                .sentence("A is a rabbit")
            .then().logRuleName().build();
            rete.registerRule(rule2);
			
			var result = Object.keys(rete.rules);
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0]=="A is vicious", `result=${result[0]}`);
			assert.ok(result[1]=="A is gentle", `result=${result[1]}`);

			var result = Object.keys(rete.alphaNodeIndexes);
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0]=="A is a tiger", `result=${result[0]}`);
			assert.ok(result[1]=="A is a rabbit", `result=${result[1]}`);

			var result = rete.alphaNodeIndexes["A is a rabbit"].out
			assert.ok(result.length===1, result.length);
			assert.ok(result[0].rule.name=="A is gentle", result[0].rule.name);

			var result = rete.alphaNodeIndexes["A is a tiger"].out
			assert.ok(result.length===1, result.length);
			assert.ok(result[0].rule.name=="A is vicious", result[0].rule.name);
		});
	});


});



// WEBPACK FOOTER //
// ./C:/Users/chekjen/git/evo3-expert/src/expert/Rete.qunit.js


// WEBPACK FOOTER //
// ./C:/Users/chekjen/git/evo3-expert/src/expert/Rete.qunit.js


// WEBPACK FOOTER //
// ./C:/Users/chekjen/git/evo3-expert/src/expert/Rete.qunit.js